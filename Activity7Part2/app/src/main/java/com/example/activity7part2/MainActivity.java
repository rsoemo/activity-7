package com.example.activity7part2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity
{
    Button btn_web, btn_email, btn_dial, btn_call, btn_text, btn_maps;
    EditText et_data;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_web = findViewById(R.id.btn_web);
        btn_email = findViewById(R.id.btn_email);
        btn_dial = findViewById(R.id.btn_dial);
        btn_call = findViewById(R.id.btn_call);
        btn_text = findViewById(R.id.btn_text);
        btn_maps = findViewById(R.id.btn_maps);
        et_data = findViewById(R.id.et_data);


        btn_web.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                openWebPage( et_data.getText().toString() );
            }
        });

        btn_email.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                openEmail(et_data.getText().toString());
            }
        });

        btn_dial.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                openDial(et_data.getText().toString());
            }
        });

        btn_call.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                openCall(et_data.getText().toString());
            }
        });

        btn_text.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                openText(et_data.getText().toString());
            }
        });

        btn_maps.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Uri mapuri = Uri.parse("geo:0,0?q=" + et_data.getText().toString());
                showMap(mapuri);
            }
        });
    }


    public void openWebPage(String data)
    {
        if (!data.startsWith("http://") || !data.startsWith("https://"))
        {
            data = "http://" + data;
        }

        Uri webPage = Uri.parse(data);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, webPage);
        startActivity(launchBrowser);
        /*if (launchBrowser.resolveActivity(getPackageManager()) != null)
        {
            startActivity(launchBrowser);
        }*/ //Was in tutorial, but caused app to not launch web browser
    }

    public void openEmail(String data)
    {
        Uri emailAddress = Uri.parse("mailto:" + data);
        Intent launchEmail = new Intent(Intent.ACTION_VIEW);
        launchEmail.setData(emailAddress);
        startActivity(launchEmail);
    }

    public void openDial(String data)
    {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + data));
        startActivity(intent);
    }

    public void openCall(String data)
    {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + data));
        startActivity(intent);
    }

    public void openText(String data)
    {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:" + data));
        intent.putExtra("sms_body", "hello there");
        //intent.putExtra(Intent.EXTRA_STREAM, attachment);
        startActivity(intent);
    }

    public void showMap(Uri geoLocation) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        startActivity(intent);
    }
}